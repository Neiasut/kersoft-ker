<?php

namespace ker\exception;

use Exception;

class ExceptionGetController extends Exception implements KerException
{
    private static $typesError = [
        'nc' => 'No controller',
        'nac' => 'No action in controller',
        'nca' => 'Not allowed controller for user'
    ];
    private $typeError;
    private $controllerName;
    private $actionName;
    private $path;

    /**
     * ExceptionGetController constructor.
     * @param string $typeError
     * @param string string $path
     * @param string string $controllerName
     * @param string $actionName
     * @throws Exception
     */
    public function __construct($typeError, $path = '', $controllerName = '', $actionName = '')
    {
        if ($this->checkTypeError($typeError)) {
            Exception::__construct('Не найден контроллер для адреса: ' . $path);
            $this->message = self::$typesError[$typeError];
            if ($controllerName) {
                $this->controllerName = $controllerName;
            }
            if ($actionName) {
                $this->actionName = $actionName;
            }
            if ($path) {
                $this->path = $path;
            }

            $this->typeError = $typeError;
        } else {
            throw new Exception('Не существует такого типа ошибки!');
        }
    }

    public static function checkTypeError($typeError)
    {
        return isset(self::$typesError[$typeError]);
    }

    public function getTypeError()
    {
        return $this->typeError;
    }
}
