<?php

namespace ker\exception;

interface KerException
{
    public function getTypeError();
    public static function checkTypeError($typeError);
}
