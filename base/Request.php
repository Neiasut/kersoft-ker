<?php
/**
 * Created by PhpStorm.
 * User: Hedin
 * Date: 12.09.2017
 * Time: 17:46
 */

namespace ker\base;

use Ker;
use ker\base\Url;

class Request
{
    public static function checkAjax()
    {
        return isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            !empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
    }
}
