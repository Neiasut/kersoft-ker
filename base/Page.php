<?php

namespace ker\base;

use Ker;

class Page
{
    private static $pageConfig = [
        'viewport' => 'width=device-width, initial-scale=1, maximum-scale=1'
    ];

    public static function setBaseGlobalVariables()
    {
        Ker::$app->twig->setGlobalVariable('viewport', self::$pageConfig['viewport']);
    }
}