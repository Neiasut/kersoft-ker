<?php

namespace ker\base;

use Ker;
use ker\base\response\Response;
use ker\base\Url as Url;
use ker\form\FormKer;
use ker\templateEngine\KerTwig;

class Application
{
    private $config = [];
    public $chainControllers = [];
    public $resources;
    public $assets;
    public $twig;
    public $formElements;
    public $response;
    /**
     * @var DB
     */
    public $db;
    /**
     * @var User
     */
    public $user;

    /**
     * Application constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        $this->config = $config;
        Ker::$app = $this;

        Ker::setAlias('@controllers', Path::getProjectPath() . $this->getElemConfigByKey('controllersPath'));
        Ker::setAlias('@models', Path::getProjectPath() . $this->getElemConfigByKey('modelsPath'));
        Ker::setAlias('@assets', Path::getProjectPath() . $this->getElemConfigByKey('assetsPath'));

        $this->response = new Response();
        $this->resources = new Resources();

        $this->twig = new KerTwig();
        $this->twig->options = [
            'auto_reload' => true,
            'debug' => true
        ];
        Ker::$app->twig->init();

        $this->formElements = new FormKer();
        $this->user = new User('anonymous');
    }

    public function getConfig()
    {
        return $this->config;
    }

    public function run()
    {
        Page::setBaseGlobalVariables();
        View::bootstrap(Url::getURI());
    }

    public function getElemConfigByKey($key)
    {
        if (isset($this->config[$key])) {
            return $this->config[$key];
        }
        return false;
    }

    public function addToChainControllers($info)
    {
        array_push($this->chainControllers, $info);
    }

    public function getChainControllers()
    {
        return $this->chainControllers;
    }
}
