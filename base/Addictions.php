<?php

namespace ker\base;

class Addictions
{
    /**
     * @var string[]
     */
    private $listAssets = [];
    /**
     * @var string[]
     */
    private $listFiles = [];

    /**
     * Addictions constructor.
     * @param string[] $listAssets
     * @param string[] $listFiles
     */
    public function __construct($listAssets = [], $listFiles = [])
    {
        $this->listFiles = $listFiles;
        $this->listAssets = $listAssets;
    }

    /**
     * @return string[]
     */
    public function getListAssets()
    {
        return $this->listAssets;
    }

    /**
     * @return string[]
     */
    public function getListFiles()
    {
        return $this->listFiles;
    }

    /**
     * @param Addictions[] $arrAddictions
     * @return Addictions
     */
    public static function mergeAddictions($arrAddictions)
    {
        $listAssets = [];
        $listFiles = [];
        $notVoidAll = false;

        /**
         * @var Addictions $addiction
         */
        foreach ($arrAddictions as $addiction) {
            if (!$addiction->checkVoid()) {
                $notVoidAll = true;
                $listAssets = mergeArrays($listAssets, $addiction, 'getListAssets');
                $listFiles = mergeArrays($listFiles, $addiction, 'getListFiles');
            }
        }
        if ($notVoidAll) {
            $listAssets = removeDuplicates($listAssets);
            $listFiles = removeDuplicates($listFiles);
        }

        return new Addictions($listAssets, $listFiles);
    }

    public function checkVoid()
    {
        return count($this->getListFiles()) < 1 && count($this->getListAssets()) < 1;
    }
}

/**
 * @param $arr
 * @param Addictions $addictions
 * @param string $method
 * @return array
 */
function mergeArrays($arr, $addictions, $method)
{
    return array_merge($arr, $addictions->$method());
}

/**
 * @param array $arr
 * @return array
 */
function removeDuplicates($arr)
{
    return array_unique($arr, SORT_REGULAR);
}
