<?php

namespace ker\base;

class RenderContainer
{
    protected static $typeAvailable = ['html', 'json'];
    protected $data;
    protected $type;

    public function __construct($data, $type)
    {
        if (array_search($type, self::$typeAvailable) !== false) {
            $this->type = $type;
            $this->data = $data;
        } else {
            throw new \Error('Нет такого типа данных');
        }
    }

    public function getData()
    {
        return $this->data;
    }

    public function getType()
    {
        return $this->type;
    }
}
