<?php

namespace ker\base;

class Files
{
    public static function getInfoFromFile($filePath, $echoError = true)
    {
        /*$info = false;
        try {
            if (self::issetFile($filePath)) {
                $info = include_once $filePath;
            }
        } catch (\Exception $e) {
            if ($echoError) {
                echo $e->getMessage();
            }
            return false;
        } finally {
            return $info;
        }*/
        if (self::issetFile($filePath)) {
            $path_info = pathinfo($filePath);
            $extension = $path_info['extension'];
            switch ($extension) {
                case 'json':
                    $str = file_get_contents($filePath);
                    return json_decode($str, true);
                    break;
                case 'php':
                    $info = include_once $filePath;
                    return $info;
                    break;
                default:
                    return file_get_contents($filePath);
            }
        }
        return false;
    }

    public static function issetFile($filePath)
    {
        return file_exists($filePath);
    }

    public static function includeFile($filePath)
    {
        $path = Path::getProjectPath() . '/' . $filePath;
        if (file_exists($path)) {
            include $path;
        }
    }

    public static function scanDirectories($rootDir, $allData = array())
    {
        // set filenames invisible if you want
        $invisibleFileNames = array(".", "..", ".htaccess", ".htpasswd");
        // run through content of root directory
        $dirContent = scandir($rootDir);
        foreach ($dirContent as $key => $content) {
            // filter all files not accessible
            $path = $rootDir.'/'.$content;
            if (!in_array($content, $invisibleFileNames)) {
                // if content is file & readable, add to array
                if (is_file($path) && is_readable($path)) {
                    // save file name with path
                    $allData[] = $path;
                    // if content is a directory and readable, add path and name
                } elseif (is_dir($path) && is_readable($path)) {
                    // recursive callback to open new directory
                    $allData = self::scanDirectories($path, $allData);
                }
            }
        }
        return $allData;
    }

    public static function getAllNameFiles($rootDir, $withRootDir = false)
    {
        $files = self::scanDirectories($rootDir);

        foreach ($files as $key => $path) {
            $pathh = $path;
            if (!$withRootDir) {
                $pathh = preg_replace("/\.\w+$/", "", $path);
            }
            $files[$key] = str_replace($rootDir.'/', '', $pathh);
        }

        return $files;
    }
}
