<?php

namespace ker\base;

use Ker;
use ker\base\Resource as Resource;

abstract class Asset
{
    private $addictions = [];

    public function __construct()
    {
        $this->addictions = $this->addAddictions();
        $this->addResources();
    }

    abstract public function addResources();

    /**
     * @return Addictions
     */
    abstract public function addAddictions();

    public static function includeAsset($name)
    {
        try {
            $fileNamespace = 'assets\\' . $name . 'Asset';
            new $fileNamespace();
        } catch (\Error $e) {
            echo $e->getMessage();
        }
    }

    public static function includeAssets($arrayAssets)
    {
        foreach ($arrayAssets as $asset) {
            self::includeAsset($asset);
        }
    }

    /**
     * @param $path
     * @param Addictions $addictions
     * @param array $attributes
     * @param bool $bottom
     */
    public function addJS($path, $addictions, $attributes = [], $bottom = true)
    {
        Ker::$app->resources->addJSFile(
            $path,
            Addictions::mergeAddictions([
                $addictions, $this->addictions
            ]),
            [$this->getNameClassFromString()],
            $attributes,
            $bottom
        );
    }

    /**
     * @param string $path
     * @param Addictions $addictions
     * @param array $attributes
     */
    public function addCSS($path, $addictions, $attributes = [])
    {
        Ker::$app->resources->addCSSFile(
            $path,
            Addictions::mergeAddictions([$addictions, $this->addictions]),
            [$this->getNameClassFromString()],
            $attributes
        );
    }

    public static function getFilesByNameAsset($nameAsset)
    {
        $result = array_filter(
            Ker::$app->resources->getListFilesUnsorted(),
            function (Resource $file) use ($nameAsset) {
                return array_search($nameAsset, $file->assets) !== false;
            }
        );
        if (count($result)) {
            return $result;
        }
        throw new \Error('No asset with name: ' . $nameAsset);
    }

    private function getNameClassFromString()
    {
        $name = explode(
            '\\',
            get_class($this)
        );
        return str_replace(
            'Asset',
            '',
            array_pop($name)
        );
    }
}
