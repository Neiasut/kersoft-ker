<?php

/**
 * @param string $data
 */
function htmlOutput($data)
{
    header('Content-Type: text/html; charset=utf-8');
    echo $data;
}

/**
 * @param $data
 */
function JSONOutput($data)
{
    header('Content-Type: application/json');
    echo json_encode($data);
}