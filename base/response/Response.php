<?php

namespace ker\base\response;

class Response
{
    protected $types = [];

    public function __construct()
    {
        include_once __DIR__ . DIRECTORY_SEPARATOR . 'response_functions.php';
        try {
            $this->addType('html', 'htmlOutput', false);
            $this->addType('JSON', 'JSONOutput', false);
        } catch (\Exception $e) {
            echo 'Error in base response type adding';
        }
    }

    /**
     * @param string $name
     * @param callable $callback
     * @param bool $useThrow
     * @throws \Exception
     */
    public function addType($name, $callback, $useThrow = true)
    {
        if ($this->issetTypeResponse($name) && $useThrow) {
            throw new \Exception('Type response with name "'.$name.'" already exist!');
        }
        $this->types[$name] = $callback;
    }

    /**
     * @param string $name
     * @return bool|mixed
     */
    public function getCallbackByName($name)
    {
        if (!$this->issetTypeResponse($name)) {
            return false;
        }
        return $this->types[$name];
    }

    /**
     * @param string $name
     * @param $data
     * @throws \Exception
     */
    public function run($name, $data)
    {
        if (!$this->issetTypeResponse($name)) {
            throw new \Exception('Type response with name "'.$name.'" already exist!');
        }
        $callable = \Closure::fromCallable($this->getCallbackByName($name));
        call_user_func($callable, $data);
    }

    /**
     * @param string $name
     * @return bool
     */
    public function issetTypeResponse($name)
    {
        return isset($this->types[$name]);
    }
}
