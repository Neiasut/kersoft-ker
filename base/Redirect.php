<?php
/**
 * Created by PhpStorm.
 * User: Hedin
 * Date: 13.09.2017
 * Time: 10:49
 */

namespace ker\base;

use ker\base\Url;

class Redirect
{
    public static function checkCurrentUri(){
        $path = Url::getURI();
        /*
        if ($path[strlen($path) - 1] == '/' && $path !== '/'){
            $path = substr($path, 0, -1);
            self::redirect301($path);
        }
        */
    }

    public static function redirect301($url){
        $url = 'http://'.$_SERVER['HTTP_HOST'].$url;
        header("Location: {$url}", true, 302);
        die();
    }

    public static function redirect($url){
        $url = 'http://'.$_SERVER['HTTP_HOST'].$url;
        header("Location: {$url}", true);
        die();
    }
}