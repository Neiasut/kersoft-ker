<?php

namespace ker\base;

use ker\base\Resource as Resource;

class Resources
{
    /**
     * @var Resource[] array
     */
    private $listFiles;
    private $listAssets = [];

    public function __construct()
    {
    }

    public function getListFilesUnsorted()
    {
        return $this->listFiles;
    }

    /**
     * @param string $path
     * @param string[] $asset
     * @param Addictions $addictions
     * @param array $attributes
     * @param bool $bottom
     */
    public function addJSFile($path, $addictions, $asset = [], $attributes = [], $bottom = true)
    {
        $this->addToListFile(
            $path,
            $addictions,
            $asset,
            $attributes,
            $bottom,
            'JS'
        );
    }

    /**
     * @param $path
     * @param string[] $asset
     * @param Addictions $addictions
     * @param array $attributes
     */
    public function addCSSFile($path, $addictions, $asset = [], $attributes = [])
    {
        $this->addToListFile(
            $path,
            $addictions,
            $asset,
            $attributes,
            false,
            'CSS'
        );
    }

    public function addInlineJS($text, $bottom)
    {
        $uniqId = uniqid();
        $this->listFiles[$uniqId] = new Resource(
            $uniqId,
            new Addictions(),
            [],
            [],
            $bottom,
            'JS',
            true,
            $text
        );
    }

    public function addInlineCSS($text)
    {
        $uniqId = uniqid();
        $this->listFiles[$uniqId] = new Resource(
            $uniqId,
            new Addictions(),
            [],
            [],
            false,
            'CSS',
            true,
            $text
        );
    }

    private function addToListFile($path, $addictions, $assets, $attributes, $bottom, $type)
    {
        if (isset($this->listFiles[$path]) && $this->listFiles[$path] instanceof Resource) {
            $this->listFiles[$path]->mergeConfigurations(
                $path,
                $addictions,
                $assets,
                $attributes,
                $bottom,
                $type,
                false,
                ''
            );
        } else {
            $this->listFiles[$path] = new Resource(
                $path,
                $addictions,
                $assets,
                $attributes,
                $bottom,
                $type,
                false,
                ''
            );
        }
        if (count($assets)) {
            $this->addAssetNameToStore($assets);
        }
    }

    public function getListFiles($type, $inline, $bottom)
    {
        if ($inline) {
            return array_filter(
                $this->listFiles,
                function (Resource $file) use ($type, $bottom) {
                    return $file->type === $type && $file->bottom === $bottom && $file->inline === true;
                }
            );
        }

        $result = $this->sortFiles(
            array_filter(
                $this->listFiles,
                function (Resource $file) use ($type, $bottom) {
                    return $file->type === $type && $file->bottom === $bottom && $file->inline !== true;
                }
            )
        );
        $this->sortFilesByWeight($result);

        return $result;
    }

    private function sortFiles($arrFiles)
    {
        $cloneArrFiles = $arrFiles;
        uasort($cloneArrFiles, function (Resource $a, Resource $b) {
            $aVoid = $a->addictions->checkVoid();
            $bVoid = $b->addictions->checkVoid();
            return $aVoid !== $bVoid && $bVoid === true ? 1 : -1;
        });
        $i = 0;
        /**
         * @var Resource $file
         */
        foreach ($cloneArrFiles as &$file) {
            $file->weight = $i;
            $i++;
        }
        foreach ($cloneArrFiles as $path => &$file) {
            $arrayWeightDepends = getArrayWeightsFromArrayPaths(
                getAllUniqueFiles($cloneArrFiles[$path]),
                $cloneArrFiles
            );
            if (count($arrayWeightDepends)) {
                $maxWeightDepend = max($arrayWeightDepends);
                if ($maxWeightDepend > $file->weight) {
                    $file->weight = $maxWeightDepend + 1;
                }
            }
        }
        return $cloneArrFiles;
    }

    /**
     * @param Resource[] $arrFiles
     */
    private function sortFilesByWeight(&$arrFiles)
    {
        uasort($arrFiles, function (Resource $a, Resource $b) {
            return $a->weight - $b->weight;
        });
    }

    /**
     * @param string[] $arrAssets
     */
    private function addAssetNameToStore($arrAssets)
    {
        foreach ($arrAssets as $nameAsset) {
            if (array_search($nameAsset, $this->listAssets) === false) {
                array_push($this->listAssets, $nameAsset);
            };
        }
    }

    public function renderListAsset($js = true, $bottom = true)
    {
        $str = '';
        if ($js) {
            $filesJS = $this->getListFiles('JS', false, $bottom);
            foreach ($filesJS as $fileJS) {
                $str .= Resource::renderFileJS($fileJS);
            }
            foreach ($this->getListFiles('JS', true, $bottom) as $inlineJS) {
                $str .= $inlineJS->inlineText;
            }
        } else {
            $filesCSS = $this->getListFiles('CSS', false, false);
            foreach ($filesCSS as $fileCSS) {
                $str .= Resource::renderFileCSS($fileCSS);
            }
            foreach ($this->getListFiles('CSS', true, false) as $inlineCSS) {
                $str .= $inlineCSS->inlineText;
            }
        }

        return $str;
    }
}

/**
 * @param string[] $arrPaths
 * @param Resource[] $changedListFiles
 * @return integer[]
 */
function getArrayWeightsFromArrayPaths($arrPaths, $changedListFiles)
{
    $arrWeights = [];
    foreach ($arrPaths as $path) {
        array_push($arrWeights, $changedListFiles[$path]->weight);
    }
    return $arrWeights;
}

function getAllUniqueFiles(Resource $resource)
{
    $resultWithDuplicate = [];
    getAllFiles($resource, $resultWithDuplicate);
    return array_unique($resultWithDuplicate, SORT_REGULAR);
}

function getAllFiles(Resource $resource, Array &$array)
{
    $array = array_merge($array, $resource->addictions->getListFiles());
    foreach ($resource->addictions->getListAssets() as $asset) {
        $listFiles = Asset::getFilesByNameAsset($asset);
        $array = array_merge($array, array_keys($listFiles));
        foreach ($listFiles as $file) {
            getAllFiles($file, $array);
        }
    }
}
