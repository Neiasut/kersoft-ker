<?php

namespace ker\base;

class Attributes
{
    public static function checkAttributes($arr)
    {
        if (!is_array($arr)) {
            $arr = [];
        }
        if (!isset($arr['class'])) {
            $arr['class'] = [];
        }

        return $arr;
    }

    public static function addClassToAttributes($attr, $class = '')
    {
        $attr = self::checkAttributes($attr);
        if (is_string($class)) {
            $class = explode(' ', $class);
        };
        $attr['class'] = array_merge($attr['class'], $class);

        return $attr;
    }

    public static function removeClassFrommAttr($attr, $class)
    {
        $attr = self::checkAttributes($attr);
        if (is_string($class)) {
            $class = explode(' ', $class);
        };

        $attr['class'] = array_values(array_diff($attr['class'], $class));

        return $attr;
    }

    public static function addAttr($attr, $addLIstAttributes)
    {
        $attr = self::checkAttributes($attr);
        return array_merge($attr, $addLIstAttributes);
    }

    public static function removeAttr($attr, $removeListAttributes)
    {
        $attr = self::checkAttributes($attr);
        foreach ($removeListAttributes as $attributeToDelete)
        {
            if (isset($attr[$attributeToDelete])) {
                unset($attr[$attributeToDelete]);
            }
        }

        return $attr;
    }

    public static function listAttributes($attributes = [])
    {
        $str = '';
        $i = false;

        if (is_array($attributes)){
            foreach ($attributes as $name_attr => $attribute){
                if ($i)
                    $str .= ' ';
                else
                    $i = true;

                if ($name_attr == 'class'){
                    if (count($attributes['class'])){
                        $class_str = implode(' ', $attributes['class']);
                        $str .= 'class="' . $class_str . '"';
                    }
                } else {
                    if (is_array($attribute)){
                        if (isset($attribute['json']) && $attribute['json'] == true){
                            $str .= $name_attr . "='" . $attribute['data'] . "'";
                        }
                    } else {
                        $str .= $name_attr . '="' . $attribute . '"';
                    }
                }
            }
        }

        return $str;
    }

    public static function setProperty($object, $massKeys, $setValue, $slice = false) {
        $timeObj = $object;
        $lengthMasKey = count($massKeys);
        $i = 0;
        $nonFind = false;
        $onLastIsset = &$timeObj;
        $lastKey = false;
        foreach ($massKeys as $key){
            if (is_array($onLastIsset) && array_key_exists($key, $onLastIsset)){
                // find last isset elem of chain keys
                $onLastIsset = &$onLastIsset[$key];
                $lastKey = $key;
            } else {
                // сохранения количества существующих ключей
                $nonFind = $i;
                break;
            }
            $i += 1;
        }

        //выборка не существующих ключей
        $nonIssetMasKeys = array_slice($massKeys, $nonFind, $lengthMasKey - $i);

        //добавление несуществующих элементов
        $ukaz = &$onLastIsset;
        foreach ($nonIssetMasKeys as $key){
            $ukaz[$key] = [];
            $ukaz = &$ukaz[$key];
        }

        if ($slice){
            if (is_array($setValue)){
                $ukaz = array_merge ($ukaz, $setValue );
            } else {
                $ukaz[] = $setValue;
            }
        } else {
            $ukaz = $setValue;
        }

        return $timeObj;
    }

    public static function getProperty($object, $massKeys) {
        $findedElem = NULL;
        $timeObj = $object;
        $onLastIsset = &$timeObj;
        $i = 0;

        foreach ($massKeys as $key){
            if (is_array($onLastIsset) && array_key_exists($key, $onLastIsset)){
                $onLastIsset = &$onLastIsset[$key];
                $i += 1;
            } else {
                break;
            }
        }

        if ($i == count($massKeys)){
            $findedElem = $onLastIsset;
        }

        return $findedElem;
    }

    public static function deleteProperty($object, $massKeys) {
        $timeObj = $object;

        deleteItemOnKeysChain($timeObj, $massKeys);

        return $timeObj;
    }
}

function deleteItemOnKey( &$array, $keyDelete )
{
    foreach( $array as $key => $val ){
        if( is_array($val) ){
            deleteItemOnKey($array[$key], $key);
        }elseif( $key===$keyDelete ){
            unset($array[$key]);
        }
    }
}

function deleteItemOnKeysChain(&$array, $keysArray){
    if (count($keysArray) > 1){
        $cutArray = array_slice ($keysArray,1);
        $key = $keysArray[0];
        if (isset($array[$key])){
            deleteItemOnKeysChain($array[$key], $cutArray);
        }
    } elseif (isset($array[$keysArray[0]])) {
        unset($array[$keysArray[0]]);
    }
}