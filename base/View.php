<?php


namespace ker\base;

use Ker;
use ker\exception\ExceptionGetController;

class View
{
    /**
     * @param $data
     */
    public static function outputData($data)
    {
        $dataUse = $data;
        $name = '';

        if ($data instanceof RenderContainer) {
            $dataUse = $data->getData();
            $name = $data->getType();
        }
        if (is_string($data)) {
            $dataUse = strval($data);
            $name = 'html';
        }
        try {
            Ker::$app->response->run($name, $dataUse);
        } catch (\Throwable $e) {
            echo 'Error in data';
        }
    }

    /**
     * @param $uri
     * @throws \Exception
     */
    public static function bootstrap($uri, $log = [])
    {
        array_push($log, $uri);
        $data = null;
        try {
            $data = Controller::getInfoFromControllerByURI($uri);
        } catch (ExceptionGetController $e) {
            $uri404 = Ker::$app->getElemConfigByKey('systemPage')['404'];
            $uri403 = Ker::$app->getElemConfigByKey('systemPage')['403'];
            if ($e->getTypeError() === 'nca') {
                if ($uri !== $uri403) {
                    self::bootstrap($uri403, $log);
                }
            }
            if ($uri !== $uri404) {
                self::bootstrap($uri404, $log);
            } else {
                try {
                    if (array_search($uri403, $log) !== false) {
                        Ker::$app->response->run(
                            'html',
                            '<h2>Error 403. Not allow to page.</h2>'
                        );
                    } else {
                        Ker::$app->response->run(
                            'html',
                            '<h2>Error 404. No page.</h2>'
                        );
                    }
                } catch (\Throwable $e) {
                    echo 'Error in bootstrap';
                }
            }
            die();
        }
        self::outputData($data);
    }
}
