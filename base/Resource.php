<?php


namespace ker\base;

use ker\base\Addictions as Addictions;
use ker\base\Attributes as Attributes;

class Resource
{
    public $path;
    /**
     * @var Addictions $addictions
     */
    public $addictions;
    public $assets = [];
    public $attributes = [];
    public $bottom = true;
    public $inline = false;
    public $type = 'JS';
    public $inlineText = '';
    public $weight = 0;

    public function __construct($path, $addictions, $assets, $attributes, $bottom, $type, $inline, $inlineText)
    {
        $this->path = $path;
        $this->addictions = $addictions;
        $this->assets = $assets;
        $this->attributes = $attributes;
        $this->bottom = $bottom;
        $this->type = $type;
        $this->inline = $inline;
        $this->inlineText = $inlineText;
    }

    /**
     * @param string $path
     * @param Addictions $addictions
     * @param string[] $assets
     * @param array $attributes
     * @param boolean $bottom
     * @param string $type
     * @param bool $inline
     * @param string $inlineText
     */
    public function mergeConfigurations($path, $addictions, $assets, $attributes, $bottom, $type, $inline, $inlineText)
    {
        $this->path = $path;
        $this->addictions = Addictions::mergeAddictions([
            $this->addictions,
            $addictions
        ]);
        $this->assets = Resource::mergeAssets($this->assets, $assets);
        $this->attributes = $attributes;
        $this->bottom = $bottom;
        $this->type = $type;
        $this->inline = $inline;
        $this->$inlineText = $inlineText;
    }

    private static function mergeAssets($assetArr1, $assetArr2)
    {
        return array_unique(array_merge($assetArr1, $assetArr2), SORT_REGULAR);
    }

    /**
     * @param Resource $js
     * @return string
     */
    public static function renderFileJS($js)
    {
        $attributes = Attributes::checkAttributes($js->attributes);
        $attributes = Attributes::addAttr($attributes, [
            'src' => $js->path
        ]);

        return "<script " . Attributes::listAttributes($attributes) . "></script>\r\n";
    }

    /**
     * @param Resource $css
     * @return string
     */
    public static function renderFileCSS($css)
    {
        $attributes = Attributes::checkAttributes($css->attributes);
        $attributes = Attributes::addAttr($attributes, [
            'href' => $css->path,
            'rel' => 'stylesheet'
        ]);

        return "<link " . Attributes::listAttributes($attributes) . "/>\r\n";
    }
}
