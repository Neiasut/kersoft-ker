<?php
/**
 * Created by PhpStorm.
 * User: Hedin
 * Date: 11.09.2017
 * Time: 16:02
 */

namespace ker\base;


class Model
{
    protected $parameters = [];

    public function __construct($parameters = [])
    {
        $this->parameters = $parameters;
    }

    /**
     * This function may call only in Actions Controller
     * @param $class
     * @return bool|mixed
     */
    public function getData($class, $action = false) {
        $info = Controller::getClassName($class);
        $nameController = Controller::shortControllerName($info);
        $nameAction = Controller::$defaultAction;
        if (is_string($action)) {
            $nameAction = Controller::shortActionName($action);
        }
        return Files::getInfoFromFile(
            Path::getBDPagesPath('/' . $nameController . '/actions/' . $nameAction . '.php')
        );
    }

    public function getAdditionalData(){
        return [];
    }

    protected function getValueParameterByKey($key){
        return $this->parameters[$key];
    }
}