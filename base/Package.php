<?php
/**
 * Created by PhpStorm.
 * User: Hedin
 * Date: 11.09.2017
 * Time: 16:51
 */

namespace ker\base;


abstract class Package
{
    public $name = '';
    public $object = [];

    public function __construct($name)
    {
        $this->name = $name;
        //\Ker::$app->setPackage($name, $this);
    }

    public static function getPackageByName($name){
        $package = (\Ker::$app->getPackageByName($name));
        if ($package){
            return $package->object;
        }
        return $package;
    }
}