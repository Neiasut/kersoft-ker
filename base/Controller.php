<?php

namespace ker\base;

use Ker;
use ker\exception\ExceptionGetController;
use ker\templateEngine\Extensions;

abstract class Controller
{
    public static $defaultAction = 'index';
    protected $pathTemplates;
    protected $renderPageTemplate = 'layouts/page.html.twig';
    protected $permissions;
    protected $allowedMethods = [];

    public function __construct($pathTemplates, $renderPageTemplate = false, $permissions = [])
    {
        $this->pathTemplates = $pathTemplates;
        $this->setAllowToController($permissions);
        if ($renderPageTemplate !== false) {
            $this->renderPageTemplate = $renderPageTemplate;
        }
    }

    public function checkAction($id)
    {
        if ($id === '') {
            $id = self::$defaultAction;
        }

        $methodName = $id;

        if (method_exists($this, $methodName)) {
            $method = new \ReflectionMethod($this, $methodName);
            if ($method->isPublic() && $method->getName() === $methodName) {
                return $methodName;
            }
        }

        return false;
    }

    public function renderPage($arrayRegion, $type = 'html', $variables = [], $customPath = false)
    {
        $usePath = $this->renderPageTemplate;
        if ($customPath !== false) {
            $usePath = $customPath;
        }
        return Controller::renderTemplate(
            $usePath,
            false,
            array_merge(
                [
                    'page_regions' => $this->prepareParamsRegions($arrayRegion)
                ],
                $variables
            ),
            $type
        );
    }

    public static function renderTemplate($path, $addResources = true, $variables = [], $type = 'html')
    {
        $content = Ker::$app->twig->render(
            $path,
            $variables
        );
        if ($addResources) {
            $top = Ker::$app->resources->renderListAsset(false)
                . Ker::$app->resources->renderListAsset(true, false);
            $bottom = Ker::$app->resources->renderListAsset(false, true)
                . Ker::$app->resources->renderListAsset(true, true);
            $content = $top . $content . $bottom;
        }
        return new RenderContainer(
            $content,
            $type
        );
    }

    protected function prepareParamsRegions($arrParams)
    {
        $result = [];
        foreach ($arrParams as $key => $param) {
            if (is_array($param) && isset($param['path'])) {
                $result[$key] = Ker::$app->twig->render(
                    $param['path'],
                    isset($param['variables']) && is_array($param['variables'])
                        ? $param['variables']
                        : []
                );
            }
            if (is_string($param)) {
                $result[$key] = $param;
            }
        }

        return $result;
    }

    protected function getNameClass()
    {
        return Controller::getClassName($this);
    }

    public static function getClassName($class)
    {
        $classNamespace = get_class($class);
        $arrParts = explode('\\',  $classNamespace);
        return array_pop($arrParts);
    }

    /**
     * Function for check allow Users to controller. Now - true for all
     * @param $nameAction
     * @return bool
     */
    protected function checkAllowed($nameAction)
    {
        $userType = Ker::$app->user->getType();
        $typeUsersForAllContent = Ker::$app->getElemConfigByKey('typeUsersForAllContent');
        if (array_search($userType, $typeUsersForAllContent) !== false) {
            return true;
        }
        if (Ker::$app->user->checkUserAllowByArrayTypes($this->getAllowedTypeUserForMethod($nameAction))) {
            return true;
        }
        return false;
    }

    public function getRenderPath()
    {
        return $this->pathTemplates;
    }

    //Functions from Run

    /**
     * @param $uri
     * @return mixed
     * @throws \Exception
     */
    public static function getInfoFromControllerByURI($uri)
    {
        $controllerInfo = self::checkValidUri($uri);

        if (is_array($controllerInfo)) {
            return self::getInfoFromController($controllerInfo['controller'], $controllerInfo['nameAction']);
        }

        throw $controllerInfo;
    }

    /**
     * @param $nameController
     * @param bool $action
     * @return mixed
     * @throws ExceptionGetController
     */
    public static function getInfoFromControllerByName($nameController, $action = false)
    {
        $controllerInfo = self::checkValidController($nameController, $action);

        if (is_array($controllerInfo)) {
            return self::getInfoFromController($controllerInfo['controller'], $controllerInfo['nameAction']);
        }

        throw $controllerInfo;
    }

    /**
     * @param Controller $controller
     * @param bool|string $action
     * @return mixed
     * @throws ExceptionGetController
     */
    protected static function getInfoFromController($controller, $action = false)
    {
        if (!$controller->checkAllowed($action)) {
            throw new ExceptionGetController('nca');
        }
        return $controller->$action();
    }

    /**
     * @param string $nameController
     * @return Controller|boolean
     */
    protected static function checkIssetController($nameController)
    {
        try {
            $fileNamespace = 'controllers\\' . $nameController;
            /**
             * @var Controller $controllerObject
             */
            return new $fileNamespace();
        } catch (\Error $e) {
            return false;
        }
    }

    //from request

    /**
     * @param $uri
     * @return array|ExceptionGetController
     * @throws \Exception
     */
    public static function checkValidUri($uri)
    {
        $useUri = self::getPrettyUri($uri);
        $parts = self::parseUri($useUri);
        return self::checkValidController($parts['controller'], $parts['action']);
    }

    protected static function generateUri($controller, $action = '')
    {
        if (!boolval($action)) {
            return $controller;
        }
        return $controller . '/' . strval($action);
    }

    /**
     * @param $nameController
     * @param $nameAction
     * @return array|ExceptionGetController
     * @throws \Exception
     */
    public static function checkValidController($nameController, $nameAction = null)
    {
        $pureControllerName = self::pureControllerName($nameController);
        $controller = self::checkIssetController($pureControllerName);
        if ($controller === false) {
            return new ExceptionGetController(
                'nc',
                self::generateUri($nameController, $nameAction),
                $pureControllerName
            );
        }
        $actionName = self::pureActionName(is_null($nameAction) ? self::$defaultAction : $nameAction);
        $checkActionInController = $controller->checkAction($actionName);
        if (!$checkActionInController) {
            return new ExceptionGetController(
                'nac',
                self::generateUri($nameController, $nameAction),
                $pureControllerName,
                $actionName
            );
        }
        return resultCheckController(
            $pureControllerName,
            $actionName,
            $controller
        );
    }

    public static function parseUri($uri)
    {
        $args = Url::args($uri);
        return [
            'controller' => $args[0],
            'action' => isset($args[1]) ? $args[1] : null
        ];
    }

    private static function getPrettyUri($prettyUri)
    {
        $result = $prettyUri;

        foreach (Ker::$app->getElemConfigByKey('prettyUrlControllers') as $changing => $toChange) {
            if ($toChange === $prettyUri) {
                $result = $changing;
                break;
            }
        }

        return $result;
    }

    public static function pureControllerName($name)
    {
        if (strpos($name, 'Controller') === false) {
            $name = $name . 'Controller';
        }
        $name[0] = mb_strtoupper($name[0]);
        return $name;
    }

    public static function shortControllerName($name)
    {
        if (strpos($name, 'Controller') !== false) {
            $name = str_replace('Controller', '', $name);
        }
        $name[0] = mb_strtolower($name[0]);
        return $name;
    }

    public static function pureActionName($name)
    {
        if (strpos($name, 'action') === false) {
            $name[0] = mb_strtoupper($name[0]);
            $name = 'action' . $name;
        }
        return $name;
    }

    public static function shortActionName($name)
    {
        if (strpos($name, 'action') !== false) {
            $name = str_replace('action', '', $name);
        }
        $name[0] = mb_strtolower($name[0]);
        return $name;
    }

    protected static function currentControllerName($name)
    {
        if (Url::useShortUrl()) {
            return self::shortControllerName($name);
        }
        return self::pureControllerName($name);
    }

    protected static function currentActionName($name)
    {
        if (Url::useShortUrl()) {
            return self::modifyRootActionByString(
                self::shortActionName($name)
            );
        }
        return self::pureActionName($name);
    }

    public static function modifyRootActionByString($name)
    {
        if (Url::useShortUrl() && $name === 'index') {
            return '';
        }
        return $name;
    }

    private function setAllowToController($typesUser)
    {
        $validUsersTypes = array_filter($typesUser, function ($type) {
            return User::checkTypeInList($type);
        });
        if (count($validUsersTypes) < 1) {
            $validUsersTypes = User::getAllTypesWithRules();
        }
        $this->permissions = $validUsersTypes;
    }

    private function setAllowToMethod($pureMethodName, $typesUser)
    {
        $validUsersTypes = array_filter($typesUser, function ($type) {
            return User::checkTypeInList($type);
        });
        $this->allowedMethods[$pureMethodName] = $validUsersTypes;
    }

    protected function setAllowToArrayMethods($arrMethodsAllow)
    {
        foreach ($arrMethodsAllow as $action => $typesUser) {
            if (is_string($action) && is_array($typesUser)) {
                $this->setAllowToMethod($action, $typesUser);
            }
        }
    }

    protected function getAllowedTypeUserForMethod($pureActionName)
    {
        if (!isset($this->allowedMethods[$pureActionName])) {
            return $this->permissions;
        }
        return array_unique(
            array_merge(
                $this->allowedMethods[$pureActionName],
                $this->permissions
            ),
            SORT_REGULAR
        );
    }
}

/**
 * @param string $name
 * @param string $action
 * @param Controller $controller
 * @return array
 */
function resultCheckController($name, $action, $controller)
{
    return [
        'nameController' => $name,
        'nameAction' => $action,
        'controller' => $controller
    ];
}