<?php

namespace ker\base;

class Url
{
    public static function useShortUrl()
    {
        return \Ker::$app->getElemConfigByKey('useShortNames');
    }

    public static function checkParamInGet($name)
    {
        return isset($_GET[$name]) ? $_GET[$name] : false;
    }

    public static function arg($numb = -1, $path = false)
    {
        if (!$path) {
            $path = parse_url(self::requestUrl())['path'];
        }
        $arg = explode('/', $path);
        array_shift($arg);

        if ($numb != -1) {
            return $arg[$numb];
        }
        return $arg;
    }

    public static function getURI()
    {
        return ltrim(parse_url(self::requestUrl())['path'], '/');
    }

    public static function args($path = false)
    {
        if (!$path) {
            $path = parse_url(self::requestUrl())['path'];
        }
        return explode('/', ltrim($path, '/'));
    }

    public static function requestUrl($all = true)
    {
        $result = '';
        $default_port = 80;

        if (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS']=='on')) {
            $result .= 'https://';
            $default_port = 443;
        } else {
            $result .= 'http://';
        }

        $result .= $_SERVER['SERVER_NAME'];

        if ($_SERVER['SERVER_PORT'] != $default_port) {
            $result .= ':'.$_SERVER['SERVER_PORT'];
        }

        if ($all) {
            $result .= $_SERVER['REQUEST_URI'];
        }

        return $result;
    }

    public static function goToUri($uri)
    {
        header('Location: ' . self::requestUrl(false) . '/' . $uri);
        exit;
    }

    public static function strReplaceOnce($search, $replace, $text)
    {
        return str_replace_once($search, $replace, $text);
    }
}

function str_replace_once($search, $replace, $text)
{
    $pos = strpos($text, $search);
    return $pos!==false ? substr_replace($text, $replace, $pos, strlen($search)) : $text;
}
