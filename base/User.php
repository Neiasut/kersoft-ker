<?php

namespace ker\base;

use Ker;

class User
{
    private static $types = [
        [
            'type' => 'anonymous',
            'weight' => 0
        ],
        [
            'type' => 'user',
            'weight' => 1
        ],
        [
            'type' => 'redactor',
            'weight' => 2
        ],
        [
            'type' => 'admin',
            'weight' => 3
        ]
    ];

    private $type;

    public function __construct($type)
    {
        $this->setType($type);
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public static function checkTypeInList($nameType)
    {
        foreach (self::$types as $infoAboutType) {
            if ($infoAboutType['type'] === $nameType) {
                return true;
            }
        }
        return false;
    }

    public static function getAllTypes()
    {
        return array_map(function ($element) {
            return $element['type'];
        }, self::$types);
    }

    public static function getAllTypesWithRules()
    {
        return array_map(function ($element) {
            return $element['type'];
        }, array_filter(self::$types, function ($element) {
            return $element['weight'] > 0;
        }));
    }

    public function checkUserAllowByArrayTypes($arrTypes)
    {
        return array_search($this->getType(), $arrTypes) !== false;
    }
    /*
        public $userInfo;
        private $typeUser = [0];

        public function __construct()
        {
            if (isset($_COOKIE["PHPSESSID"])) {
                session_start();
                $userInfo = self::checkUser($_SESSION['userInfo'], true);
                if ($userInfo) {
                    $this->userInfo = $userInfo;
                    $infoUser = $userInfo['infoUser'];

                    if (isset($infoUser['typeUser'])) {
                        $this->typeUser = $infoUser['typeUser'];
                    }
                } else {
                    $this->killCookieSessionImportant();
                }
            } else {
                $this->killCookieSessionImportant();
            }
        }

        public function killCookieSessionImportant()
        {
            if (isset($_COOKIE['PHPSESSID'])) {
                unset($_COOKIE['PHPSESSID']);
                setcookie('PHPSESSID', null, -1, '/');
            }
            $this->typeUser = [0];
        }

        public static function getHashCode($pass)
        {
            return password_hash($pass, PASSWORD_DEFAULT);
        }

        public static function checkPass($passwordReal, $password, $hashPass)
        {
            if ($hashPass) {
                return password_verify($passwordReal, $password);
            }
            return $passwordReal === htmlspecialchars($password);
        }

        public static function setUserCookie($name, $password)
        {
            $userInfo = [
                'login' => $name,
                'password' => self::getHashCode($password)
            ];
            setcookie('userInfo', serialize($userInfo), time()+60*60, '/');
        }

        public static function setUserLoginInfo($name, $password)
        {
            $userInfo = [
                'login' => $name,
                'password' => self::getHashCode($password)
            ];
            session_start();
            $_SESSION['userInfo'] = $userInfo;
        }

        public static function clearUserCookie()
        {
            unset($_COOKIE['userInfo']);
            setcookie('userInfo', null, -1, '/');
        }

        public static function clearUserSession()
        {
            session_start();
            session_unset();
            session_destroy();
        }

        public static function checkUser($data, $passHash = true)
        {
            // isset cookie
            if (isset($data)) {
                $userInfo = $data;
                $login = $userInfo['login'];
                $password = $userInfo['password'];
                // isset file with info user
                if (self::checkIssetUserByLogin($login)) {
                    $info = Files::getInfoFromFile(self::getPathUserFile($login));
                    // check cookie pass and real pass
                    if (self::checkPass($info['infoUser']['password'], $password, $passHash)) {
                        return $info;
                    };
                }
            }

            return false;
        }

        public static function checkIssetUserByLogin($login)
        {
            return file_exists(self::getPathUserFile($login));
        }

        private static function getPathUserFile($login)
        {
            return Path::getUserFilesPath() . '/' . $login . '_user.php';
        }

        public function getTypeUser()
        {
            return $this->typeUser;
        }

        public function checkGlobalPermission()
        {
            $globPerm = self::getGlobalPermission();

            return self::checkAllowToType($globPerm, $this->typeUser);
        }

        public static function getGlobalPermission()
        {
            return Ker::$app->getElemConfigByKey('globalAllowToContent');
        }


        public static function checkAllowToType($rights, $typeAllow)
        {
            foreach ($typeAllow as $right) {
                if (array_search($right, $rights) !== false) {
                    return true;
                }
            }

            return false;
        }

        public function getUserInfo()
        {
            return $this->userInfo;
        }

        public function getInfoAboutUser()
        {
            return $this->getUserInfo()['infoUser'];
        }
        */
}
