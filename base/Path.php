<?php

namespace ker\base;

use Ker;

class Path
{
    public static function getProjectPath()
    {
        return Ker::getAlias('@ROOT');
    }

    public static function getVendorPath($path = false)
    {
        $base = Path::getProjectPath() . '/vendor';
        return ($path) ? $base . '/' . $path : $base;
    }

    public static function getControllerPath()
    {
        return Ker::getAlias('@controllers');
    }

    public static function getWebPath()
    {
        return self::getProjectPath() . Ker::$app->getElemConfigByKey('publicPath');
    }

    public static function getViewsPath()
    {
        return self::getProjectPath() . Ker::$app->getElemConfigByKey('viewsPath');
    }

    public static function getUserFilesPath()
    {
        return self::getProjectPath() . Ker::$app->getElemConfigByKey('usersInfoPath');
    }

    public static function getBDPagesPath($path = false)
    {
        return self::getProjectPath() . '/BD/pages' . ($path ? $path : '');
    }

    public static function getBDFilesDataPath()
    {
        return self::getProjectPath() . '/BD/filesData';
    }

    public static function getBDProductsPath()
    {
        return self::getProjectPath() . '/BD/products';
    }

    public static function getBDTemplatesPayments()
    {
        return self::getProjectPath() . '/BD/templatesPayments';
    }
}
