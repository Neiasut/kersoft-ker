<?php

namespace ker\templateEngine\helpers;

class RandomColor
{
    private static $lastColorGenerate = false;
    private static $colorMap = array(
        'E57373',
        'F06292',
        'BA68C8',
        '9575CD',
        '7986CB',
        '64B5F6',
        '4FC3F7',
        '4DD0E1',
        '4DB6AC',
        '81C784',
        'AED581',
        'DCE775',
        //'FFF176',
        'FFD54F',
        'FFB74D',
        'FF8A65',
        'A1887F',
        'E0E0E0',
        '90A4AE',
        '37AEB2',
        'FFAA02',
        'FDB841',
        'BEC4CA',
        '37AEB2',
        '179CA2',
        'A4DBDC',
        '76C9CB',
        'FECF7B',
        'FDB033',
        'FD9148',
        'E68442',
        '145081',
        '134976',
        'EA526F',
        'D54B65',
        '009CA3',
    );

    /**
     * @param string $data
     * @return string
     */
    public static function colorGenerate($data = '')
    {
        $s = 0;
        foreach (str_split( sha1($data) ) as $i => $c){
            $h = ord($c);
            $s += $h;
        }

        if (self::$lastColorGenerate){
            $colorMap = array_diff(self::$colorMap, [self::$lastColorGenerate]);
        } else {
            $colorMap = self::$colorMap;
        }


        $cl = $colorMap[ $s % count($colorMap) ];

        $dec = hexdec($cl);
        $hex = dechex($dec & 0xFFFFFF);
        $colorHEX = str_pad($hex, 6,'0', STR_PAD_LEFT);
        self::$lastColorGenerate = strtoupper($colorHEX);

        return '#'. $colorHEX;
    }

    public static function payment_leter_generator($data = '') {
        $pos = 0;
        if( ($p = mb_strpos($data, '"')) !== false ) $pos = $p+1;
        if( ($p = mb_strpos($data, '«')) !== false ) $pos = $p+1;
        if( ($p = mb_strpos($data, "'")) !== false ) $pos = $p+1;
        $leter = mb_substr($data, $pos, 1);

        return $leter;
    }

}