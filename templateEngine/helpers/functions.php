<?php
/**
 * Created by PhpStorm.
 * User: Hedin
 * Date: 15.09.2017
 * Time: 16:54
 */

function moneyFormat($money, $viewDelimiter = 2, $typeDelimiter = ','  ,$moneyType = '₽'){
    $numb = number_format($money, $viewDelimiter, $typeDelimiter, ' ');
    $str_numb = $numb . '';
    $pos = strpos($str_numb, $typeDelimiter);
    if ($pos === false){
        $lowMoney = '';
    } else {
        $lowMoney = substr($str_numb, $pos );
        $str_numb = substr_replace($str_numb, '', $pos);
    }
    if ($numb < 0){
        $str_numb = str_replace('-', '− ', $str_numb);
    }
    $str_numb .= "<span class=\"x2font\">$lowMoney $moneyType</span>";

    return $str_numb;
}

function dateRU($param, $time=0) {
    if(intval($time)==0)$time=time();
    $MonthNames=array("января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря");
    if(strpos($param,'F')===false) return date($param, $time);
    else return date(str_replace('F',$MonthNames[date('n',$time)-1],$param), $time);
}

function generateRandomString($length = 8){
    $chars = 'abdefhiknrstyzABDEFGHKNQRSTYZ23456789';
    $numChars = strlen($chars);
    $string = '';
    for ($i = 0; $i < $length; $i++) {
        $string .= substr($chars, rand(1, $numChars) - 1, 1);
    }
    return $string;
}