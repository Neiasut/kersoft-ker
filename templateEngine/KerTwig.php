<?php

namespace ker\templateEngine;

use Ker;
use ker\base\Path;
use Twig_Environment;
use Twig_Loader_Filesystem;
use Twig_Extension_Debug;

class KerTwig
{

    public $options = [];
    public $functions = [];
    public $filters = [];

    /**
     * @var \Twig_Environment twig environment object that renders twig templates
     */
    public $twig;

    public function init()
    {
        $loader = new Twig_Loader_Filesystem(Path::getViewsPath());
        $this->twig = new Twig_Environment($loader, array_merge([
            'cache'       => Path::getProjectPath() . '/compilation_cache'
        ], $this->options));

        $this->twig->addExtension(new Twig_Extension_Debug());
        $this->twig->addExtension(new Extensions());
        $this->twig->addGlobal('globalVars', []);
    }

    public function setGlobalVariable($name, $value)
    {
        $vars = $this->twig->getGlobals()['globalVars'];
        $vars[$name] = $value;
        $this->twig->addGlobal('globalVars', $vars);
    }

    public function getGlobalVariables()
    {
        return $this->twig->getGlobals()['globalVars'];
    }

    public function getGlobalVariable($nameVariable)
    {
        return $this->twig->getGlobals()['globalVars'][$nameVariable];
    }

    public function render($filePathInViews, $params = [])
    {
        return $this->twig->render($filePathInViews, $params);
    }

    public function renderRaw($filePathInViews, $params = [])
    {
        return new \Twig_Markup(
            $this->twig->render($filePathInViews, $params),
            $this->twig->getCharset()
        );
    }

    /**
     * Adds custom functions
     * @param array $functions @see self::$functions
     * @throws \Exception
     */
    public function addFunctions($functions)
    {
        $this->addCustom('Function', $functions);
    }

    /**
     * Adds custom filters
     * @param array $filters @see self::$filters
     * @throws \Exception
     */
    public function addFilters($filters)
    {
        $this->addCustom('Filter', $filters);
    }

    /**
     * Adds custom function or filter
     * @param string $classType 'Function' or 'Filter'
     * @param array $elements Parameters of elements to add
     * @throws \Exception
     */
    private function addCustom($classType, $elements)
    {
        $classFunction = 'Twig_Simple' . $classType;

        foreach ($elements as $name => $func) {
            $twigElement = null;

            $this->twig->{'add'.$classType}(
                new $classFunction(
                    $name,
                    $func[0],
                    isset($func[1]) && is_array($func[1])
                        ? $func[1]
                        : []
                )
            );
        }
    }
}
