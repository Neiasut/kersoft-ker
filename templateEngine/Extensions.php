<?php

namespace ker\templateEngine;

use Ker;
use ker\base\Files;
use ker\base\Path;
use ker\templateEngine\helpers\RandomColor;

class Extensions extends \Twig_Extension
{
    public $widgets = [];
    public $widgetsPath = '';

    public function __construct()
    {
        require_once __DIR__ . '/helpers/filters.php';
        require_once __DIR__ . '/helpers/functions.php';
        $this->createListWidgets();
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'ker-twig';
    }

    /**
     * @inheritdoc
     */
    public function getFunctions()
    {
        $options = [
            'is_safe' => ['html'],
        ];
        $functions = [
            new \Twig_SimpleFunction('listAttributes', '\ker\base\Attributes::listAttributes', $options),
            new \Twig_SimpleFunction('setProperty', '\ker\base\Attributes::setProperty', $options),
            new \Twig_SimpleFunction('getProperty', '\ker\base\Attributes::getProperty', $options),
            new \Twig_SimpleFunction('deleteProperty', '\ker\base\Attributes::deleteProperty', $options),
            new \Twig_SimpleFunction('topResources', [$this, 'topResources'], $options),
            new \Twig_SimpleFunction('bottomResources', [$this, 'bottomResources'], $options),
            new \Twig_SimpleFunction('moneyFormat', 'moneyFormat', $options),
            new \Twig_SimpleFunction('dateRU', 'dateRU', $options),
            new \Twig_SimpleFunction('getRandomColor', [$this, 'getRandomColor'], $options),
            new \Twig_SimpleFunction('generateLetter', [$this, 'generateLetter'], $options),
            new \Twig_SimpleFunction('getThisPath', [$this, 'getThisPath'], $options),
            new \Twig_SimpleFunction('getBeforePath', [$this, 'getBeforePath'], $options),
            new \Twig_SimpleFunction('*_widget', [$this, 'widgetCreate'], $options),
            new \Twig_SimpleFunction('generateRandomString', 'generateRandomString', $options),
            new \Twig_SimpleFunction('loadFile', [$this, 'loadFile'], $options),
            new \Twig_SimpleFunction('getFromFilesData', [$this, 'getFromFilesData'], $options)
        ];

        $options = array_merge($options, [
            'needs_context' => true,
        ]);
        $functions[] = new \Twig_SimpleFunction('addInlineJs', [$this, 'addInlineJs'], $options);
        $functions[] = new \Twig_SimpleFunction('addInlineCSS', [$this, 'addInlineCSS'], $options);

        return $functions;
    }

    public function getFilters()
    {
        $options = [
            'is_safe' => ['html'],
        ];

        $filters = [
            new \Twig_SimpleFilter('checkAttributes', '\ker\base\Attributes::checkAttributes', $options),
            new \Twig_SimpleFilter('addClass', '\ker\base\Attributes::addClassToAttributes', $options),
            new \Twig_SimpleFilter('removeClass', '\ker\base\Attributes::removeClassFrommAttr', $options),
            new \Twig_SimpleFilter('addAttr', '\ker\base\Attributes::addAttr', $options),
            new \Twig_SimpleFilter('removeAttr', '\ker\base\Attributes::removeAttr', $options)
        ];

        return $filters;
    }

    public function formElement()
    {
        return 'form';
    }

    protected function createListWidgets()
    {
        $this->widgetsPath = Path::getViewsPath() . '/widgets';

        if (is_dir($this->widgetsPath)) {
            $results = scandir($this->widgetsPath);

            foreach ($results as $result) {
                if ($result === '.' or $result === '..') {
                    continue;
                };

                if (is_dir($this->widgetsPath . '/' . $result)) {
                    array_push($this->widgets, $result);
                }
            }
        }
    }

    public function widgetCreate($name, $data = [], $attributes = [])
    {
        if (in_array($name, $this->widgets)) {
            try {
                $widget = Ker::$app->twig->render("widgets/$name/$name.html.twig", [
                    'data' => $data,
                    'attributes' => $attributes
                ]);

                return $widget;
            } catch (\Exception $e) {
                return '<div>Нет такого виджета: ' . $name . '<br />' . $e->getMessage() . '</div>';
            }
        } else {
            return '<div>Нет такого виджета: ' . $name . '</div>';
        }
    }

    /**
     * @param $context
     * @param \Twig_Markup $text
     * @param boolean $bottom
     */
    public function addInlineJs($context, $text, $bottom = true)
    {
        Ker::$app->resources->addInlineJS($text->__toString(), $bottom);
    }

    /**
     * @param $context
     * @param \Twig_Markup $text
     */
    public function addInlineCSS($context, $text)
    {
        Ker::$app->resources->addInlineCSS($text->__toString());
    }

    public function topResources()
    {
        $css = Ker::$app->resources->renderListAsset(false);
        $js = Ker::$app->resources->renderListAsset(true, false);
        return $css . $js;
    }

    public function bottomResources()
    {
        return Ker::$app->resources->renderListAsset();
    }

    public function getRandomColor()
    {
        return RandomColor::colorGenerate();
    }

    public function generateLetter($string = '')
    {
        return RandomColor::payment_leter_generator($string);
    }

    public function getThisPath($twig)
    {
        return dirname($twig);
    }

    public function getBeforePath($twig)
    {
        return dirname($this->getThisPath($twig));
    }

    public function loadFile($path = '')
    {
        return file_get_contents(Path::getProjectPath() . $path);
    }

    public function getFromFilesData($path) {
        return Files::getInfoFromFile(
            Path::getBDFilesDataPath() . $path
        );
    }
}