<?php

namespace ker\web;

use Ker;
use ker\form\FormKer;

class Application extends \ker\base\Application
{
    private $extendLibraryRun = [
        /*
        [
            'static' => false,
            'class' => 'customTwigAdds\CustomTwigAdds',
            'call' => 'addCustomTwigFunctions'
        ],
        [
            'static' => false,
            'class' => 'formKer\FormKer'
        ]
        */
    ];

    protected $form;

    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->extendLibraryRun();
    }

    private function extendLibraryRun()
    {

        foreach ($this->extendLibraryRun as $library) {
            try {
                $params = (isset($library['params']) && count($library['params'])) ? $library['params'] : [];
                if ($library['static']) {
                    $callElem = [$library['class'], $library['call']];
                    call_user_func_array($callElem, $params);
                } else {
                    $object = new \ReflectionClass($library['class']);
                    $objectClass = $object->newInstance($params);
                    if (isset($library['call'])) {
                        call_user_func_array([$objectClass, $library['call']], $params);
                    }
                }
            } catch (\Exception $e) {
                echo $e->getMessage();
            }
        }
    }
}
