<?php

namespace ker\form\elements;

use Ker;
use ker\base\Files;
use ker\base\Path;
use ker\base\Url;

class Elements
{
    protected $path = 'modules/formKer';
    protected $elements = [];

    public function __construct()
    {
        $this->elements = $this->checkElements();
    }

    /**
     * @inheritdoc
     */
    public function __get($varName)
    {
        return new \Twig_Markup('<div>Some widget</div>', 'UTF-8');
    }

    /**
     * @inheritdoc
     */
    public function __isset($varName)
    {
        $parts = Elements::prepareName($varName);
        $resultFind = $this->findElementInList($parts['name'], $parts['themes']);
        return $resultFind !== false;
    }

    /**
     * @inheritdoc
     */
    public function __call($varName, $arguments = [])
    {
        $parts = Elements::prepareName($varName);
        $resultFind = $this->findElementInList($parts['name'], $parts['theme']);
        if ($resultFind === false) {
            return new \Twig_Markup('<div style="color: red;">Нет такого элемента формы! '.$varName.'</div>', 'UTF-8');
        }
        return Ker::$app->twig->renderRaw(
            $resultFind->getFilePath(),
            [
                'data' => isset($arguments[0]) ? $arguments[0] : [],
                'attributes' => isset($arguments[1]) ? $arguments[1] : [],
                'attributesWrapper' => isset($arguments[2]) ? $arguments[2] : [],
                'themes' => isset($arguments[3]) ? $arguments[3] : []
            ]
        );
    }

    protected static function prepareName($name)
    {
        $parts = explode('_', $name, 3);
        return [
            'name' => $parts[0],
            'theme' => isset($parts[1]) ? $parts[1] : ''
        ];
    }

    protected function findElementInList($elementName, $theme = '')
    {
        $findElement = findElement($this->elements, $elementName);

        if ($findElement === false) {
            return false;
        }

        if ($theme !== '') {
            return findElement($findElement->themes, $theme);
        }

        return $findElement;
    }

    protected function getRelativePath($relative)
    {
        return $this->path . DIRECTORY_SEPARATOR . $relative;
    }

    protected function getAbsPath($relative)
    {
        return Path::getViewsPath() . DIRECTORY_SEPARATOR . $relative;
    }

    /**
     * @return ElementForm[]
     */
    protected function checkElements()
    {
        $path = $this->getRelativePath('elements');
        $listElements = $this->checkElementsInPath($path, true);
        foreach ($listElements as $element) {
            $element->themes = $this->checkElementsInPath($element->path, false, $element->name);
        }
        return $listElements;
    }

    /**
     * @param $path
     * @param boolean $root
     * @param bool $nameUse
     * @return ElementForm[]
     */
    protected function checkElementsInPath($path, $root, $nameUse = false)
    {
        $absPath = $this->getAbsPath($path);
        $pathUse = $absPath . '/*';
        $dirs = glob($pathUse, GLOB_ONLYDIR);
        $issetFiles = [];
        foreach ($dirs as $pathDir) {
            $theme = basename($pathDir);
            $name = $nameUse === false ? basename($pathDir) : $nameUse;
            $useFile = ElementForm::generateFilePath($pathDir, $name);
            if (Files::issetFile($useFile)) {
                $relativePath = Url::strReplaceOnce($this->getAbsPath(''), '', $pathDir);
                array_push(
                    $issetFiles,
                    new ElementForm($name, $root, $relativePath, $theme, [])
                );
            }
        }

        return $issetFiles;
    }
}

/**
 * @param ElementForm[] $where
 * @param string $name
 * @return ElementForm|bool
 */
function findElement($where, $name)
{
    $result = array_filter($where, function ($element) use ($name) {
        return $element->selfTheme === $name;
    });
    return count($result) > 0 ? reset($result) : false;
}
