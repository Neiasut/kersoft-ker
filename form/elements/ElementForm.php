<?php


namespace ker\form\elements;

class ElementForm
{
    public $name;
    public $path;
    public $themes = [];
    public $selfTheme = '';
    public $root = false;

    /**
     * ElementForm constructor.
     * @param $name
     * @param $path
     * @param ElementForm[] $themes
     */
    public function __construct($name, $root, $path, $selfTheme, $themes)
    {
        $this->name = $name;
        $this->selfTheme = $selfTheme;
        $this->path = $path;
        $this->themes = $themes;
        $this->root = $root;
    }

    public function getFilePath()
    {
        return ElementForm::generateFilePath($this->path, $this->name);
    }

    public static function generateFilePath($path, $name)
    {
        return $path . DIRECTORY_SEPARATOR . $name .'.html.twig';
    }
}
