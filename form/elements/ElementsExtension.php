<?php

namespace ker\form\elements;

use Ker;

class ElementsExtension extends \Twig_Extension implements \Twig_Extension_GlobalsInterface
{
    protected $elements;

    public function __construct()
    {
        $this->elements = new Elements();
    }

    public function getName()
    {
        return 'FormKerElementExtension';
    }

    public function getGlobals()
    {
        return array(
            'formElement' => $this->elements
        );
    }
}