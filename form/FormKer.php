<?php

namespace ker\form;

use Ker;
use ker\form\elements\ElementsExtension;

class FormKer
{
    public function __construct()
    {
        Ker::$app->twig->twig->addExtension(new ElementsExtension());
    }
}
